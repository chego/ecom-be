// Libraries
const Route = require('fastify-plugin')

// Services
const List = require('../service/product/list')
const Fetch = require('../service/product/fetch')

module.exports = Route((Server, Options, Next) => {

    Server.route({
        url: `${Options.url}s`,
        method: 'GET',
        handler: List
    })

    Server.route({
        url: `${Options.url}`,
        method: 'GET',
        handler: Fetch,
        schema: {
            query: {
                type: 'object',
                properties: {
                    id: {
                        type: 'string'
                    }
                }
            }
        }
    })

    Next()
    
})