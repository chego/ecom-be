// Libraries
const Router = require('fastify-plugin')

module.exports = Router((Server, Options, Next) => {

    Server.route({
        url: `${Options.url}`,
        method: 'GET',
        handler: (request, response) => response.code(200).send()
    })

    Next()
    
})