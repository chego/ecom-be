// Models
const Product = require('../../model/product')

module.exports = async (request, response) => {
    try {
        response.code(200).send(await Product.findOne(request.query, {
            _id: 0,
            __v: 0
        }))
    }
    catch (error) {
        response.code(500).send({
            message: 'Server has encountered an internal server error.'
        })
    }
}