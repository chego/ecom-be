// Libraries
const Lodash = require('lodash')
const FuseJS = require('fuse.js')

// Models
const Product = require('../../model/product')

module.exports = async (request, response) => {
    try {
        let products = await Product.aggregate([
            {
                $addFields: {
                    image: {
                        $arrayElemAt: [ '$images', 0 ]
                    }
                }
            },
            {
                $group: {
                    _id: '$id',
                    id: {
                        $first: '$id'
                    },
                    name: {
                        $first: '$name'
                    },
                    image: {
                        $first: '$image'
                    },
                    retailPrice: {
                        $first: '$retailPrice'
                    },
                    discountedPrice: {
                        $first: '$discountedPrice'
                    }
                }
            },
            {
                $sort: {
                    discountedPrice: request.query.order ? (Lodash.isEqual(request.query.order, 'ascending') ? 1 : -1) : 1
                }
            },
            {
                $project: {
                    _id: 0
                }
            }
        ])
        if (!Lodash.isEmpty(request.query.search)) {
            products = Lodash.map(new FuseJS(products, {
                findAllMatches: true,
                useExtendedSearch: true,
                threshold: 0,
                keys: [ 'name' ]
            }).search(request.query.search), (product) => {
                return product.item
            })
        }
        response.code(200).send(products)
    }
    catch (error) {
        response.code(500).send({
            message: 'Server has encountered an internal server error.'
        })
    }
}