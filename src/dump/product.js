// Libraries
const Fs = require('fs')
const Lodash = require('lodash')

module.exports = {
    up: async (database) => {
        let products = await database.collection('products').find({}).toArray()
        if (products.length > 0) { return }
        else {
            products = await Fs.readFileSync('productdf38641.json')
            await database.collection('products').insertMany(Lodash.map(JSON.parse(products.toString()), (input) => {
                let product = {
                    id: input.uniq_id,
                    name: input.product_name,
                    description: input.description,
                    images: input.image
                }
                if (!Lodash.isEqual(input.brand.length, 0)) { product.brand = input.brand }
                if (Lodash.isNumber(input.retail_price)) { product.retailPrice = input.retail_price }
                if (Lodash.isNumber(input.discounted_price)) { product.discountedPrice = input.discounted_price }
                if (Lodash.isNumber(input.product_rating)) { product.productRating = input.product_rating }
                if (Lodash.isNumber(input.overall_rating)) { product.overallRating = input.overall_rating }
                return product
            }))
        }
    },
    down: async () => {}
}