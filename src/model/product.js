// Utilities
const Database = require('../utility/database')

const productSchema = Database.Schema({

    id:                                 { type: String,     minlength: 32,      maxlength: 32,          required: true },
    name:                               { type: String,                                                 required: true },
    description:                        { type: String,                                                 required: true },
    images:                            [{ type: String,                                                 required: true }],
    brand:                              { type: String                                                                 },
    retailPrice:                        { type: Number,     min: 0                                                     },
    discountedPrice:                    { type: Number,     min: 0                                                     },
    productRating:                      { type: Number,     min: 0,             max: 5                                 },
    overallRating:                      { type: Number,     min: 0,             max: 5                                 }

})

productSchema.index({ id: 1 }, { unique: true })

module.exports = Database.model('Product', productSchema)