// Libraries
const Lodash = require('lodash')

// Routes
const Routes = [
    { url: '/health',  controller: require('./route/health')  },
    { url: '/product', controller: require('./route/product') }
]

module.exports.attachTo = (Server) => {
    Lodash.forEach(Routes, (Route) => {
        Server.register(Route.controller, { url: Route.url })
    })
}