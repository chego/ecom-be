// Set Environment Variables
require('dotenv').config()

// Declare Server Instance
const Server = require('fastify')({
    bodyLimit: 1048576,
    trustProxy: true,
    caseSensitive: false,
    logger: {
        level: process.env.LOG_LEVEL
    }
})

// Attach Library Plugins
Server.register(require('fastify-cors'))

// Attach Source Plugins
require('./src').attachTo(Server)

// Launch Server Instance
Server.listen(process.env.PORT, (error) => {
    if (error) {
        Server.log.fatal(error)
        process.exit(1)
    }
})