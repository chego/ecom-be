// Libraries
require('dotenv').config()

module.exports = {
    mongodb: {
        url: `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_CLUSTER}.mongodb.net`,
        databaseName: process.env.DB_NAME,
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
    },
    migrationsDir: 'src/dump',
    changelogCollectionName: 'dumps',
    migrationFileExtension: '.js'
}